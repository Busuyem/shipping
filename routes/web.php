<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shipping.order');
})->name('homepage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('shipbyair', 'ShippingController@shipByAir')->name('shipbyair');
Route::post('shipbyair', 'ShippingController@saveShippingByAir')->name('postShipByAir');
Route::get('shipbyairdetails', 'ShippingController@shipByAirDetails')->name('shipbyairdetails');
Route::get('shipbyair/checkout', 'ShippingController@checkoutByAir')->name('checkoutAir');
Route::post('shipbyair/checkout', 'ShippingController@postCheckoutByAir')->name('postCheckoutAir');

Route::get('shipbysea', "ShippingController@shipBySea")->name('shipbysea');
Route::post('shipbysea', 'ShippingController@saveShippingBySea')->name('postShipBySea');
Route::get('shipbyseadetails', 'ShippingController@shipBySeaDetails')->name('shipbyseadetails');
Route::get('shipbysea/checkout', 'ShippingController@checkoutBySea')->name('checkoutSea');
Route::post('shipbysea/checkout', 'ShippingController@postCheckoutBySea')->name('postCheckoutSea');

//Payment Routes

Route::post('/pay', 'ShippingController@redirectToGateway')->name('pay');
Route::get('/payment/callback', 'ShippingController@handleGatewayCallback');