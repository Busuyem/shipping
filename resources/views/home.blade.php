@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div>
                        <p>Welcome! <a href="{{ route('homepage') }}" class="btn btn-dark btn-sm">Ship you luggage now!</a></p>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
