@extends('layouts.app')

@section('content')
    <div class="container col-md-8 offset-2">
        <div class="header d-flex justify-content-center align-items-center mb-5 bg-dark p-2 rounded shadow text-white">
            <h3 class="font-weight-bold">Ship your loads with ease through any of the following chanels!</h3>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title ">Ship by AIR</h5>
                  <p class="card-text">This gives you the opportunity to have goods delivered to you in two days.</p>
                  <a href="{{ route('shipbyair') }}" class="btn btn-primary">Ship Now</a>
                  <a href="#" class="btn btn-secondary">Base cost <span>&#8358;</span> 50,000</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Ship by SEA</h5>
                  <p class="card-text">This gives you the opportunity to have goods delivered to you in 10 days.</p>
                  <a href="{{ route('shipbysea') }}" class="btn btn-primary">Ship Now</a>
                  <a href="#" class="btn btn-secondary">Base cost <span>&#8358;</span> 10,000</a>
                </div>
              </div>
            </div>
          </div>
    </div>
    
@endsection