@extends('layouts.app')

@section('content')

<div class="container col-md-4 offset-4">
    <form action="" method="Post" action="{{ route('postCheckoutAir') }}">
        @csrf
        <div class="mb-3">
            <h3>Your Air Shipment Cost is Displayed Below</h3>
        </div>
        <div class="mb-3">
            <label for="amount_to_pay" class="form-label"><h4>Your Order Sum Total</h4></label>
            <div class="d-flex">
                <h3><span class="p-1">&#8358;</span></h3>
                <input type="number" name="amount_to_pay" value="{{ session('total')*0.01 }}" class="form-control" readonly>
            </div>
        </div>

        <div class="mb-3">
            <input type="hidden" name="email" value="" class="form-control" readonly>
        </div>

        <div class="mb-3">
            <input type="hidden" name="address" value="" class="form-control" readonly>
        </div>

        <div class="mb-3">
           <button type="submit" class="btn btn-dark">Proceed to payment</button>
        </div>
    </form>
</div>
    
@endsection