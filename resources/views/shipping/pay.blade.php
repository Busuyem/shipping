@extends('layouts.app')

@section('content')
    <div class="container col-md-6 offset-3">
        <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
            <div class="row" style="margin-bottom:40px;">
                <div class="col-md-8 col-md-offset-2">
                    <p>
                        <div class="text-center">
                            <h3>Lagos Freight Forwarding Business</h3>
                            <h3><span class="p-1">&#8358;</span> {{ number_format(session('total')*0.01, 2) }}</h3>
                        </div>
                    </p>
                    <input type="hidden" name="email" value="{{ session('detail')->email }}"> {{-- required --}}
                    <input type="hidden" name="orderID" value="{{ session('detail')->id }}">
                    <input type="hidden" name="amount" value="{{ session('total') }}">
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="currency" value="NGN">
                    <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                    <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                    @csrf
        
                    <p>
                        <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
                            <i class="fa fa-plus-circle fa-lg"></i> Confirm Payment Now!
                        </button>
                    </p>
                </div>
            </div>
        </form>
    </div>
@endsection