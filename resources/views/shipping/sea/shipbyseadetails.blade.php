@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="col-md-8 offset-2">
            <div class="header d-flex justify-content-center align-items-center mb-5 bg-dark p-2 rounded shadow text-white">
                <h3>Your Shipping Details</h3>
            </div>
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Shipping Cost Details</h5>
                  <p class="card-text">Base Fare: <b><span>&#8358;</span> {{ number_format($shippingBySeaBaseFare, 2) }}</b></p>
                  <p class="card-text">Arrival Date:  <b>{{ $arrivalTime }}</b></p>
                  <p class="card-text">Weight of Loads/Goods: <b> {{ $shippingLoadWeight }} kg</b></p>
                  <p class="card-text">Cost per kg: <b><span>&#8358;</span> {{ number_format($shippingBySeaCostPerKg, 2) }}</b></p>
                  <p class="card-text">Shipping Cost for {{ $shippingLoadWeight }} kg loads/goods: <b><span>&#8358;</span> {{ number_format($totalWeightCost, 2) }}</b></p>
                  <p class="card-text">Total Shipping Cost by Sea: <b><span>&#8358;</span> {{  number_format($totalShippingCost, 2) }}</b></p>
                  <p class="card-text">Tax: <b><span>&#8358;</span> {{ number_format($taxOnShippingBySea, 2) }}</b></p>
                  <p class="card-text">Sum Total: <b><span>&#8358;</span> {{ number_format($groundTotalCostOfShipingBySea, 2) }}</b></p>
                  <p class="card-text">Your Name: <b>{{ $shippingBySeaDetail->first_name}} {{$shippingBySeaDetail->last_name }}</b></p>
                  <p class="card-text">Shipping Address: <b>{{ $shippingBySeaDetail->address }}</b></p>
                  <p class="card-text">Your Email Address: <b>{{ $shippingBySeaDetail->email }}</b></p>
                  <a href="{{ route('checkoutSea') }}" class="btn btn-primary">Checkout</a>
                </div>
              </div>
        </div>
    </div>
    
@endsection