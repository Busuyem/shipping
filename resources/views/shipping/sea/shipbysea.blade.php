@extends('layouts.app')

@section('content')

<div class="container col-md-6 offset-3 mb-5">
    <form action="{{ route('postShipBySea') }}" method="post">
        @csrf
        <div class="header d-flex justify-content-center align-items-center mb-5 bg-dark p-2 rounded shadow text-white">
            <h4 class="font-weight-bold">Your Shiping Details</h4>
        </div>
        <div class="mb-3">
            <label for="mode" class="form-label"><h4>Mode of Shipping</h4></label>
            <input type="text" name="mode" value="Sea" class="form-control" readonly>
        </div>
        <div class="mb-3">
            <label for="base_fare" class="form-label"><h4>Base Fare(<span>&#8358;</span>)</h4></label>
            <input type="number" name="base_fare" value="10000" class="form-control" readonly>
        </div>
        <div class="mb-3">
            <label for="first_name" class="form-label"><h4>Your First Name</h4></label>
            <input type="text" name="first_name" class="form-control @error('firstname') is-invalid @enderror" placeholder="Enter your first name">
            @error('first_name')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="last_name" class="form-label"><h4>Your Last Name</h4></label>
            <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" placeholder="Enter your last name">
            @error('last_name')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="email" class="form-label"><h4> email address</h4></label>
            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter your email address">
            @error('email')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="address" class="form-label"><h4>Your shipping address</h4></label>
            <textarea class="form-control @error('address') is-invalid @enderror" name="address"  rows="3" placeholder="Enter your shipping address"></textarea>
            @error('address')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="mb-3">
            <label for="country" class="form-label"><h4>Destination Country</h4></label>
            <input type="text" name="country" value="Nigeria" class="form-control" readonly>
        </div>

        <div class="mb-3">
            <label for="weight" class="form-label"><h4>Weight of your loads/goodsn in kg</h4></label>
            <input type="number" name="weight" class="form-control @error('weight') is-invalid @enderror" placeholder="Enter weight here">
            @error('weight')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="mb-3">
            <label for="country_from" class="form-label"><h4>Select Country of Origin</h4></label>
            <select class="form-select form-control @error('country_from') is-invalid @enderror" name="country_from">
                <option selected disabled>select</option>
                <option value="US">US</option>
                <option value="UK">UK</option>
                <option value="China">China</option>
                <option value="Germany">Germany</option>
                <option value="Canada">Canada</option>
            </select>
            @error('country_from')
                <span class="text text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="mb-3">
            <button class="btn btn-success">Submit</button>
        </div>
    </form>
</div>
    
@endsection