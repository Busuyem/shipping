@component('mail::message')

<div class="container col-md-6 offset-3">
    <p>Your payment of the sum of <b> &#8358;</span> {{ number_format(session('total'),2)  }} </b> only is successful.</p>
</div>


Thanks for patronizing us,<br>
{{ config('app.name') }}
@endcomponent
