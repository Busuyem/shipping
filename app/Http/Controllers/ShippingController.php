<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\CheckoutAir;
use App\CheckoutSea;
use App\ShippingByAir;
use App\ShippingBySea;
use App\Mail\PaymentMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Unicodeveloper\Paystack\Facades\Paystack;

class ShippingController extends Controller
{
    public function shipByAir()
    {
        return view('shipping.air.shipbyair');
    }

    public function saveShippingByAir(Request $request)
    {
        $validatedData = $this->validate($request, $this->dataToValidate());
        ShippingByAir::create($validatedData);
        return redirect()->route('shipbyairdetails');
    }

    public function shipByAirDetails()
    {
        $shippingByAirDetail = ShippingByAir::latest()->first();

        $orderDate = Carbon::now('Africa/Lagos');
    
        $arrivalTime = Carbon::parse($orderDate)->addDays(2)->format('Y-m-d H:i:s a');

        $sessionAirDetail = session()->put('detail', $shippingByAirDetail);

        if($shippingByAirDetail->country_from === "US")
        {
            $shippingByAirCostPerKg = 1500.00;

        }else if($shippingByAirDetail->country_from === "UK")
        {
            $shippingByAirCostPerKg = 800.00;

        }else
        {
            $shippingByAirCostPerKg = 10000.00;
        }

        $shippingByAirBaseFare = $shippingByAirDetail->base_fare;

        $shippingLoadWeight = $shippingByAirDetail->weight;

        $totalWeightCost = $shippingLoadWeight * $shippingByAirCostPerKg;

        $totalShippingCost = $shippingByAirBaseFare + $totalWeightCost;

        $taxOnShippingByAir = $totalShippingCost * 0.1;

        $groundTotalCostOfShipingByAir = $totalShippingCost + $taxOnShippingByAir;

        $groundTotalInKobo = $groundTotalCostOfShipingByAir*100;

        session()->put('total', $groundTotalInKobo);
        
        return view('shipping.air.shipbyairdetails', compact('shippingByAirDetail', 'shippingByAirCostPerKg', 'shippingByAirBaseFare', 'shippingLoadWeight', 'totalWeightCost', 'totalShippingCost', 'taxOnShippingByAir', 'groundTotalCostOfShipingByAir', 'arrivalTime'));
    }

    public function checkoutByAir()
    {
        return view('shipping.air.checkout');
    }

    public function postCheckoutByAir(Request $request)
    {
        $checkoutData = CheckoutAir::create([
            'email' => session('detail')->email,
            'address' => session('detail')->address,
            'amount_to_pay' => session('total')
        ]);
        
        $detail = session()->get('detail');
        $total = number_format(session()->get('total'),2);
        return view('shipping.pay', compact('detail', 'total'));;
    }

   
    public function shipBySea()
    {
        return view('shipping.sea.shipbysea');
    }


    public function saveShippingBySea(Request $request)
    {
        $validatedData = $this->validate($request, $this->dataToValidate());
        ShippingBySea::create($validatedData);
        return redirect()->route('shipbyseadetails');
    }


    public function shipBySeaDetails()
    {
        $shippingBySeaDetail = ShippingBySea::latest()->first();

        $orderDate = Carbon::now('Africa/Lagos');

        $arrivalTime = Carbon::parse($orderDate)->addDays(20)->format('Y-m-d H:i:s a');
        
        $sessionDetail = session()->put('detail',$shippingBySeaDetail);
        
        if($shippingBySeaDetail->country_from === "US")
        {
            $shippingBySeaCostPerKg = 1500;

        }else if($shippingBySeaDetail->country_from === "UK")
        {
            $shippingBySeaCostPerKg = 800;

        }else
        {
            $shippingBySeaCostPerKg = 10000;
        }

      

        $shippingBySeaBaseFare = $shippingBySeaDetail->base_fare;

        $shippingLoadWeight = $shippingBySeaDetail->weight;

        $totalWeightCost = $shippingLoadWeight * $shippingBySeaCostPerKg;

        $totalShippingCost = $shippingBySeaBaseFare + $totalWeightCost;

        $taxOnShippingBySea = $totalShippingCost * 0.1;

        $groundTotalCostOfShipingBySea = $totalShippingCost + $taxOnShippingBySea;

        $groundTotalInKobo = $groundTotalCostOfShipingBySea*100;

        session()->put('total', $groundTotalInKobo);

        return view('shipping.sea.shipbyseadetails', compact('shippingBySeaDetail', 'shippingBySeaCostPerKg', 'shippingBySeaBaseFare', 'shippingLoadWeight', 'totalWeightCost', 'totalShippingCost', 'taxOnShippingBySea', 'groundTotalCostOfShipingBySea', 'arrivalTime'));
    }

    public function checkoutBySea()
    {
        return view('shipping.sea.checkout');
    }

    public function postCheckoutBySea(Request $request)
    {
        $checkoutData = CheckoutSea::create([
            'email' => session('detail')->email,
            'address' => session('detail')->address,
            'amount_to_pay' => session('total')
        ]);

        $detail = session()->get('detail');
        $total = number_format(session()->get('total'), 2);
        return view('shipping.pay', compact('detail', 'total'));
    }


    public function redirectToGateway()
    {
        try{
            return Paystack::getAuthorizationUrl()->redirectNow();
        }catch(\Exception $e) {
            return Redirect::back()->withMessage(['msg'=>'The paystack token has expired. Please refresh the page and try again.', 'type'=>'error']);
        }        
    }

    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        $userData = session()->get('detail');

        Mail::to($userData['email'])->send(new PaymentMail($userData));
        
        $user = User::latest()->first();

        $adminUser = $user->email;

        if($adminUser){

            Mail::to($adminUser)->send(new PaymentMail($userData));
        }
        
        //dd($paymentDetails);

        return redirect()->route('homepage');

        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }

    public function dataToValidate()
    {
        return [

            'mode' => 'required',
            'base_fare'=> 'required',
            'first_name'=> 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'country' => 'required',
            'weight' => 'required',
            'country_from' => 'required'

        ];
    }
}
